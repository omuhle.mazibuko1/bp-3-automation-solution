﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Knowledge_Recap
{
    internal class WebCore
    {

        public static IWebDriver driver;

        public static void initDriver()
        {
            driver = new ChromeDriver();
            driver.Url = "https://www.saucedemo.com/";
            driver.Manage().Window.Maximize();
        }

        public static IWebElement getElement(By xpath)
        {
            IWebElement elementFound = driver.FindElement(xpath);
            return elementFound;
        }

        public static void WaitForPageLoad()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5000);
        }

        public static string getCurrentPageTitle()
        {
            string actualPageTitle = driver.Title.ToString();
            return actualPageTitle;
        }

    }
}
