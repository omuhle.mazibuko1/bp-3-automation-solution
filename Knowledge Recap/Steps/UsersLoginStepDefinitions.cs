using System;
using TechTalk.SpecFlow;

namespace Knowledge_Recap
{
    [Binding]
    public class UsersLoginStepDefinitions
    {

        UserLogginIn logginIn = new UserLogginIn();

        [Given(@"user wants to access sauce demo web")]
        public void GivenUserWantsToAccessSauceDemoWeb()
        {
            WebCore.initDriver();
        }

        [When(@"user enters ""([^""]*)"" and ""([^""]*)""")]
        public void WhenUserEntersAnd(string username, string password)
        {
            logginIn.userLogin(username, password);
        }


        [Then(@"user should be logged in succesfully")]
        public void ThenUserShouldBeLoggedInSuccesfully()
        {
            logginIn.isUserLoggedIn();
        }

        [Then(@"""([^""]*)"" should be displayed to the user")]
        public void ThenShouldBeDisplayedToTheUser(string error)
        {
            logginIn.assertErrorMessage(error);
        }

    }
}
