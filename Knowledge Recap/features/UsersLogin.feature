﻿Feature: UsersLogin

Login user

@tag1
Scenario Outline: User Login
	Given user wants to access sauce demo web
	When user enters "<username>" and "<password>"
	Then user should be logged in succesfully

	Examples: 
	| username      | password                 |
	| standard_user | secret_sauce             |
	|  problem_user |  secret_sauce            |


	@tag1
Scenario Outline: Invalid User Login
	Given user wants to access sauce demo web
	When user enters "<username>" and "<password>"
	Then  "<errorMessage>" should be displayed to the user

	Examples: 
	| username     | password        | errorMessage                                                                      |
	| invalid_user | secretinv_sauce | Epic sadface: Username and password do not match any user in this service         |


	@tag1
Scenario Outline: Locked User Login
	Given user wants to access sauce demo web
	When user enters "<username>" and "<password>"
	Then  "<errorMessage>" should be displayed to the user

	Examples: 
	| username        | password        |     errorMessage                                       |
	| locked_out_user | secret_sauce    |  Epic sadface: Sorry, this user has been locked out.   |


		@tag1
Scenario Outline: Empty Credentials User Login
	Given user wants to access sauce demo web
	When user enters "<username>" and "<password>"
	Then  "<errorMessage>" should be displayed to the user

	Examples: 
	| username     | password | errorMessage                            |
	|              |          | Epic sadface: Username is required      |  





