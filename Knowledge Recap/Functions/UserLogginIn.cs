﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knowledge_Recap
{
    internal class UserLogginIn
    {
        //Functions below:
        public void userLogin(string username,string password)
        {
            WebCore.getElement(usernameField()).SendKeys(username);
            WebCore.getElement(passwordField()).SendKeys(password);
            WebCore.getElement(loginButton()).Click();
        }

        public void isUserLoggedIn()
        {
            WebCore.WaitForPageLoad();
            string title = WebCore.getCurrentPageTitle();
            string expectedPageTitle = "Swag Labs";

            if (title.Equals(expectedPageTitle))
            {
                Console.WriteLine("User logged in");
            }
            else
            {
                Assert.Fail("User was not logged In succesfully..");

            }

            Boolean isFound = WebCore.getElement(dashBoardText()).Displayed;

            if (isFound == true)
            {
                Console.WriteLine("DashBoard Text Found...");
            }
            else 
            {
                Assert.Fail("DashBoard Text NOT Found...");
            }
        }

        public void assertErrorMessage(string expectedMessage)
        {
            string actualErrorMessage = WebCore.getElement(loginErrorMessage()).Text;

            if (actualErrorMessage.Equals(expectedMessage))
            {
                Console.WriteLine("User Login Error Message Matches....");
            }
            else 
            {
                Assert.Fail("User Login Error Message Does NOT Match....");
            }
        }


        //Elements below:

        public By usernameField()
        {
            By username = By.Id("user-name");
            return username;
        }

        public By passwordField()
        {
            By pass = By.Id("password");
            return pass;
        }

        public By loginButton()
        {
            By login = By.Id("login-button");
            return login;
        }


        public By dashBoardText()
        {
            By dash = By.ClassName("app_logo");
            return dash;
        }

        public By loginErrorMessage()
        {
            By error = By.XPath("//h3[@data-test='error']");
            return error;
        }
    }
}
